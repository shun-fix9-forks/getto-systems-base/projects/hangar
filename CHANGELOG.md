# CHANGELOG

## Version : 1.0.4

- fix: push_tags : See merge request getto-systems-base/labo/hangar!14


## Version : 1.0.3

- fix: push_tags : See merge request getto-systems-base/labo/hangar!13


## Version : 1.0.2



## Version : 1.0.1



## Version : 1.0.0

- production ready! : See merge request getto-systems-base/labo/hangar!12
- fix: push_tags : See merge request getto-systems-base/labo/hangar!11


## Version : 0.4.0

- fix: push : See merge request getto-systems-labo/hangar!10


## Version : 0.3.0

- fix: ignore tag : See merge request getto-systems-labo/hangar!9
- fix: gitlab-ci : See merge request getto-systems-labo/hangar!8
- fix: update_version : See merge request getto-systems-labo/hangar!7
- fix: update_version mode : See merge request getto-systems-labo/hangar!6


## Version : 0.2.0

- fix: push_latest : See merge request getto-systems-labo/hangar!5


## Version : 0.1.3



## Version : 0.1.2



## Version : 0.1.1

- fix: gitlab-ci : See merge request getto-systems-labo/hangar!4


## Version : 0.1.0

- add: push_latest script : See merge request getto-systems-labo/hangar!3


## Version : 0.0.1


